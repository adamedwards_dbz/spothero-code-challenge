package com.spothero.domain

enum class Day(val display: String) {
    MONDAY("mon"),
    TUESDAY("tues"),
    WEDNESDAY("wed"),
    THURSDAY("thurs"),
    FRIDAY("fri"),
    SATURDAY("sat"),
    SUNDAY("sun");


    override fun toString(): String = display

    companion object {
        fun of(day: String): Day = Day.values().first { it.display.equals(other = day, ignoreCase = false) }
    }
}