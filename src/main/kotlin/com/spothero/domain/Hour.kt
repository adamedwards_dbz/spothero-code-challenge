package com.spothero.domain

import java.security.InvalidParameterException

class Hour(hour: Int, minute: Int) : Comparable<Hour> {
    private val hour: Int
    private val minute: Int

    init {
        this.hour = if (hour in MIN..HOUR_MAX) {
            hour
        } else {
            throw InvalidParameterException("Hour must be between $MIN and $HOUR_MAX")
        }
        this.minute = if (minute in MIN..MINUTE_MAX) {
            minute
        } else {
            throw InvalidParameterException("Minute must be between $MIN and $HOUR_MAX")
        }
    }

    override fun compareTo(other: Hour): Int =
        when {
            this.hour < other.hour -> -1
            this.hour > other.hour -> 1
            else -> when {
                this.minute < other.minute -> -1
                this.minute > other.minute -> 1
                else -> 0
            }
        }

    override fun toString(): String = "%02d%02d".format(hour, minute)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Hour) return false

        if (hour != other.hour) return false
        if (minute != other.minute) return false

        return true
    }

    override fun hashCode(): Int {
        var result = hour
        result = 31 * result + minute
        return result
    }

    companion object {
        private const val MIN = 0
        private const val HOUR_MAX = 23
        private const val MINUTE_MAX = 59
    }
}