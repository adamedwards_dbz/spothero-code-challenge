package com.spothero.domain

data class Rate(
    val days: Set<Day>,
    val times: Times,
    val price: Long
)