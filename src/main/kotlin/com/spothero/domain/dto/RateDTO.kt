package com.spothero.domain.dto

data class RateDTO(
    val days: String,
    val times: String,
    val price: Long
)