package com.spothero

import com.spothero.repository.RatesDAO
import com.spothero.services.RatesService
import com.spothero.web.RatesController
import io.javalin.Javalin
import mu.KotlinLogging

fun main(args: Array<String>) {
    val app = Javalin
        .create()
        .port(8080)
        .exception(Exception::class.java) { e, ctx ->
            log.error { "Error encountered when making request to ${ctx.request()}." }
            log.error { e }
        }
        .error(404) { ctx -> ctx.json("Not Found") }
        .enableStandardRequestLogging()

    val ratesDAO = RatesDAO()
    val ratesService = RatesService(ratesDAO)

    //apply routes for Rates Controller
    RatesController(app, ratesService)

    app.start()
}

private val log = KotlinLogging.logger { }