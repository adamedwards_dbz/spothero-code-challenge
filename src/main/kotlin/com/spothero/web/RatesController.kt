package com.spothero.web

import com.spothero.services.RatesService
import io.javalin.ApiBuilder.get
import io.javalin.ApiBuilder.post
import io.javalin.Javalin

class RatesController(
    server: Javalin,
    private val ratesService: RatesService
) {
    init {
        server.routes {
            get(RATES_ENDPOINT) { ctx -> ratesService.fetchRates(ctx) }

            post(RATES_ENDPOINT) { ctx -> ratesService.createRate(ctx) }
        }
    }

    companion object {
        private const val RATES_ENDPOINT = "/rates"
    }
}