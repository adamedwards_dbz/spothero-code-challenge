package com.spothero.repository

import com.spothero.domain.Day.FRIDAY
import com.spothero.domain.Day.MONDAY
import com.spothero.domain.Day.SATURDAY
import com.spothero.domain.Day.SUNDAY
import com.spothero.domain.Day.THURSDAY
import com.spothero.domain.Day.TUESDAY
import com.spothero.domain.Day.WEDNESDAY
import com.spothero.domain.Hour
import com.spothero.domain.Rate
import com.spothero.domain.dto.RateDTO

class RatesDAO {
    private val rates = mutableListOf(
        Rate(
            days = setOf(MONDAY, TUESDAY, THURSDAY),
            times = Hour(9, 0)..Hour(21, 0),
            price = 1500
        ),
        Rate(
            days = setOf(FRIDAY, SATURDAY, SUNDAY),
            times = Hour(9, 0)..Hour(21, 0),
            price = 2000
        ),
        Rate(
            days = setOf(WEDNESDAY),
            times = Hour(6, 0)..Hour(18, 0),
            price = 1750
        ),
        Rate(
            days = setOf(MONDAY, WEDNESDAY, SATURDAY),
            times = Hour(1, 0)..Hour(5, 0),
            price = 1000
        ),
        Rate(
            days = setOf(SUNDAY, TUESDAY),
            times = Hour(1, 0)..Hour(7, 0),
            price = 925
        )
    )

    fun fetchRates(): Map<String, Collection<RateDTO>> {
        val rateDtos = rates
            .map {
                RateDTO(
                    days = it.days
                        .map { it.display }
                        .toString()
                        .replace(" ", "") // remove blank spaces
                        .replace("[", "") // remove opening array bracket
                        .replace("]", ""),// remove closing array bracket
                    times = "${it.times.start}-${it.times.endInclusive}",
                    price = it.price
                )
            }

        return mapOf(RATES to rateDtos)
    }

    fun save(rate: Rate): Boolean = rates.add(rate)

    companion object {
        private const val RATES = "rates"
    }
}