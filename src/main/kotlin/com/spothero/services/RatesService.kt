package com.spothero.services

import com.spothero.domain.Day
import com.spothero.domain.Hour
import com.spothero.domain.Rate
import com.spothero.domain.dto.RateDTO
import com.spothero.repository.RatesDAO
import io.javalin.Context
import mu.KotlinLogging
import java.security.InvalidParameterException

class RatesService(
    private val ratesDao: RatesDAO
) {
    fun fetchRates(context: Context): Context = context.json(ratesDao.fetchRates())

    fun createRate(context: Context): Boolean {
        val rateDTO = context.bodyAsClass(RateDTO::class.java)

        return try {
            val rate = buildRateFromDto(rateDTO)

            if (ratesDao.save(rate)) {
                context.status(201)

                true
            } else {
                context.status(500)
                context.json("Something went wrong. Please contact an administrator.")

                false
            }
        } catch (e: Exception) {
            log.error { "Something went wrong trying to build a Rate from the passed RateDTO" }
            log.error { e }

            context.status(401)
            context.json("Bad request. Please check payload structure and try again")

            false
        }
    }

    private fun buildRateFromDto(rateDTO: RateDTO): Rate {
        val days = rateDTO.days
            .split(",")
            .map { Day.of(it) }
            .toSet()

        val startHour: Hour
        val endHour: Hour

        if (rateDTO.times.length == TIMES_LENGTH) {
            startHour = Hour(
                hour = rateDTO.times.substring(START_HOUR_START, START_HOUR_END).toInt(),
                minute = rateDTO.times.substring(START_MINUTE_START, START_MINUTE_END).toInt()
            )

            endHour = Hour(
                hour = rateDTO.times.substring(END_HOUR_START, END_HOUR_END).toInt(),
                minute = rateDTO.times.substring(END_MINUTE_START, END_MINUTE_END).toInt()
            )
        } else {
            throw InvalidParameterException("Times must appear in the format of 0000-2359")
        }

        return Rate(
            days = days,
            times = startHour..endHour,
            price = rateDTO.price
        )
    }

    companion object {
        private const val TIMES_LENGTH = 9

        private const val START_HOUR_START = 0
        private const val START_HOUR_END = 2
        private const val START_MINUTE_START = 2
        private const val START_MINUTE_END = 4

        private const val END_HOUR_START = 5
        private const val END_HOUR_END = 7
        private const val END_MINUTE_START = 7
        private const val END_MINUTE_END = TIMES_LENGTH

        private val log = KotlinLogging.logger { }
    }
}