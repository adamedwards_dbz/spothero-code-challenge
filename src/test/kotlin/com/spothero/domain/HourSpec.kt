package com.spothero.domain

import com.winterbe.expekt.expect
import com.winterbe.expekt.should
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on

object HourSpec : Spek({
    describe("Hour"){
        var subject: Hour

        context("compareTo") {
            subject = Hour(12, 30)
            var other: Hour

            on("this hour is less than other hour") {
                other = Hour(13, 30)

                it("should return -1") {
                    subject.should.be.below(other)
                }
            }

            on("this hour is greater than other hour") {
                other = Hour(11, 30)

                it("should return 1") {
                    subject.should.be.above(other)
                }
            }

            context("this hour is equal to other hour") {
                on("this minute is less than other minute") {
                    other = Hour(12, 59)

                    it("should return -1") {
                        subject.should.be.below(other)
                    }
                }

                on("this minute is greater than other minute") {
                    other = Hour(12, 0)

                    it("should return 1") {
                        subject.should.be.above(other)
                    }
                }

                on("this minute is equal to other minute") {
                    other = Hour(12, 30)

                    it("should return 0") {
                        subject.should.equal(other)
                    }
                }
            }
        }

        context("toString") {
            on("hour value of less than 10") {
                it("should pad with a 0") {
                    subject = Hour(0, 59)

                    expect(subject.toString()).to.equal("0059")
                }
            }
            on("minute value of less than 10") {
                it("should pad with a 0") {
                    subject = Hour(10, 1)

                    expect(subject.toString()).to.equal("1001")
                }
            }
        }
    }
})