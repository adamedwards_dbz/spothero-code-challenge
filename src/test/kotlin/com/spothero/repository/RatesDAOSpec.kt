package com.spothero.repository

import com.spothero.domain.Day
import com.spothero.domain.Hour
import com.spothero.domain.Rate
import com.spothero.domain.dto.RateDTO
import com.winterbe.expekt.expect
import com.winterbe.expekt.should
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it

object RatesDAOSpec : Spek({
    describe("RatesDAO") {
        var subject = RatesDAO()

        beforeEachTest {
            subject = RatesDAO()
        }

        context("fetchRates") {
            it("should return a map that contains a list of Rate objects") {
                val actual = subject.fetchRates()

                expect(actual.containsKey(RATES)).to.be.`true`
            }
        }

        context("save") {
            it("should add the passed rate to the list of Rate objects") {
                val price = 2400L

                val rate = Rate(
                    days = setOf(Day.MONDAY, Day.TUESDAY),
                    times = Hour(10, 0)..Hour(21, 0),
                    price = price
                )
                val expected = RateDTO(
                    days = "${Day.MONDAY.display},${Day.TUESDAY.display}",
                    times = "1000-2100",
                    price = price
                )

                val successful = subject.save(rate)

                successful.should.be.`true`

                val actual = subject.fetchRates().getValue(RATES).last()

                actual.should.equal(expected)
            }
        }
    }
})

private const val RATES = "rates"