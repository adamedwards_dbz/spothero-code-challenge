package com.spothero.services

import com.spothero.domain.Day
import com.spothero.domain.Hour
import com.spothero.domain.Rate
import com.spothero.domain.dto.RateDTO
import com.spothero.repository.RatesDAO
import com.winterbe.expekt.should
import io.javalin.Context
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on

object RatesServiceSpec : Spek({
    describe("RatesService") {
        var testWrapper = RatesServiceTestWrapper()

        beforeEachTest {
            testWrapper = RatesServiceTestWrapper()
        }

        context("fetchRates") {
            it("should add all rates to the context") {
                testWrapper.subject.fetchRates(testWrapper.context)

                verify { testWrapper.ratesDAO.fetchRates() }
                verify { testWrapper.context.json(any()) }
            }
        }

        context("createRate") {
            val price = 2400L

            var rateDTO: RateDTO
            var expectedRate: Rate

            on("valid RateDTO is passed") {
                it("should add the passed RateDTO to the Rates in RatesDAO " +
                    "and return true") {
                    rateDTO = RateDTO(
                        days = "${Day.MONDAY.display},${Day.TUESDAY.display}",
                        times = "1000-2100",
                        price = price
                    )

                    expectedRate = Rate(
                        days = setOf(Day.MONDAY, Day.TUESDAY),
                        times = Hour(10, 0)..Hour(21, 0),
                        price = price
                    )

                    every {
                        testWrapper.context.bodyAsClass(RateDTO::class.java)
                    } returns rateDTO

                    val actual = testWrapper.subject.createRate(testWrapper.context)

                    actual.should.be.`true`

                    verify { testWrapper.ratesDAO.save(expectedRate) }
                    verify { testWrapper.context.status(201) }
                }
            }

            context("invalid RateDTO is passed") {
                on("invalid dates param") {
                    it("should add a 401 response to the context" +
                        "and return true") {
                        rateDTO = RateDTO(
                            days = "Bruce Wayne",
                            times = "1000-2100",
                            price = price
                        )

                        every {
                            testWrapper.context.bodyAsClass(RateDTO::class.java)
                        } returns rateDTO

                        val actual = testWrapper.subject.createRate(testWrapper.context)

                        actual.should.be.`false`

                        verify(exactly = 0) { testWrapper.ratesDAO.save(any()) }
                        verify { testWrapper.context.status(401) }
                        verify { testWrapper.context.json(any<String>()) }
                    }
                }
                on("invalid times param because length is not equal to 9 characters") {
                    it("should add a 401 response to the context" +
                        "and return true") {
                        rateDTO = RateDTO(
                            days = "${Day.MONDAY.display},${Day.TUESDAY.display}",
                            times = "12-14",
                            price = price
                        )

                        every {
                            testWrapper.context.bodyAsClass(RateDTO::class.java)
                        } returns rateDTO

                        val actual = testWrapper.subject.createRate(testWrapper.context)

                        actual.should.be.`false`

                        verify(exactly = 0) { testWrapper.ratesDAO.save(any()) }
                        verify { testWrapper.context.status(401) }
                        verify { testWrapper.context.json(any<String>()) }
                    }
                }
            }

            on("failed to persist Rate to RatesDAO") {
                it("should add a 500 response to the context" +
                    "and return true") {
                    rateDTO = RateDTO(
                        days = "${Day.MONDAY.display},${Day.TUESDAY.display}",
                        times = "1000-2100",
                        price = price
                    )

                    every {
                        testWrapper.context.bodyAsClass(RateDTO::class.java)
                    } returns rateDTO

                    every {
                        testWrapper.ratesDAO.save(any())
                    } returns false

                    val actual = testWrapper.subject.createRate(testWrapper.context)

                    actual.should.be.`false`

                    verify { testWrapper.ratesDAO.save(any()) }
                    verify { testWrapper.context.status(500) }
                    verify { testWrapper.context.json(any<String>()) }
                }
            }
        }
    }
})

private class RatesServiceTestWrapper(
    val ratesDAO: RatesDAO = mockk(relaxed = true),
    val context: Context = mockk(relaxed = true)
) {
    val subject = RatesService(ratesDAO)

    init {
        every { ratesDAO.fetchRates() } returns emptyMap()
        every { ratesDAO.save(any()) } returns true
    }
}