# spothero-code-challenge
A Javalin web application designed for SpotHero.

## Requirements
Please see [REQUIREMENTS.md](REQUIREMENTS.md)

## Flavors
This application is implemented in both [Java](https://github.com/thatadamedwards/spothero-code-challenge/tree/java) and [Kotlin](https://github.com/thatadamedwards/spothero-code-challenge/tree/kotlin). 

The `master` branch is in sync with the `kotlin` branch.

## Build and Run
This application is built with Gradle. 

To test and build, run `./gradlew build`. 

Artifacts are located in `./build/libs`.

You can run the application with `java -jar ./build/libs/spothero-code-challenge-0.0.1.jar`

## Structure
```
 |-src
 |---main
 |-----java|kotlin
 |---test
 |-----java|kotlin
 |
 |build.gradle
 |gradlew
 |gradlew.bat
 |README.md
 |REQUIREMENTS.md
 |settings.gradle
```